package streamlet

import (
	"context"
	"fmt"
	"log"
	"time"
)

const (
	ElementCreating    = "Creating"
	ElementCreated     = "Created"
	ElementActivating  = "Activating"
	ElementActivated   = "Activated"
	ElementCompleting  = "Completing"
	ElementCompleted   = "Completed"
	ElementTerminating = "Terminating"
	ElementTerminated  = "Terminated"
)

type ElementType string

const (
	ProcessInstanceType            ElementType = "ProcessInstance"
	TokenType                      ElementType = "Token"
	SequenceFlowType               ElementType = "SequenceFlow"
	ExclusiveGatewayType           ElementType = "ExclusiveGateway"
	ParallelGatewayType            ElementType = "ParallelGateway"
	NoneStartEventType             ElementType = "NoneStartEvent"
	NoneIntermediateThrowEventType ElementType = "NoneIntermediateThrowEvent"
	NoneEndEventType               ElementType = "NoneEndEvent"
	TimerStartEventType            ElementType = "TimerStartEventType"
	UserTaskType                   ElementType = "UserTaskType"
	ScriptTaskType                 ElementType = "ScriptTaskType"
)

var (
	ErrDataUnmarshal = NewBPMNEngineError(nil, "")
)

func (e ElementType) String() string {
	return string(e)
}

type LifeCycleRunner interface {
	RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token)
}

type ElementState struct {
	ElementType ElementType
	Key         string
	Id          string
	Status      string
	Object      any `json:"-"`
}

type ElementStateProvider interface {
	GetElementState() ElementState
}

type StartEvent struct {
	ElementState
}

func NewNoneStartEvent(key, id string) *StartEvent {
	return &StartEvent{ElementState: ElementState{Key: key, Id: id, ElementType: NoneStartEventType}}
}

func (e *StartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *StartEvent) RefElement() any {
	return e
}

func (e *StartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	e.Status = ElementCompleted
}

type TimerStartEvent struct {
	ElementState
}

func NewTimerStartEvent(key, id string) *TimerStartEvent {
	return &TimerStartEvent{ElementState: ElementState{Key: key, Id: id, ElementType: TimerStartEventType}}
}

func (e *TimerStartEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *TimerStartEvent) RefElement() any {
	return e
}

func (e *TimerStartEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	e.Status = ElementCompleted
}

type SendTask struct {
	ElementState
}

func (s *SendTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	s.Status = ElementCompleted
	bpmnEngine.WriteState(s.ElementState)
}

type SequenceFlow struct {
	ElementState
}

func (s *SequenceFlow) GetElementState() ElementState {
	return s.ElementState
}

func NewSequenceFlow(key, id string) *SequenceFlow {
	return &SequenceFlow{ElementState{Key: key, Id: id, ElementType: SequenceFlowType}}
}

func (s *SequenceFlow) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	s.Status = ElementCompleted
	bpmnEngine.WriteState(s.ElementState)
}

type ExclusiveGateway struct {
	ElementState
	Handler     func() ElementStateProvider
	NextElement ElementStateProvider
}

func NewExclusiveGateway(key, id string, handler func() ElementStateProvider) *ExclusiveGateway {
	return &ExclusiveGateway{ElementState: ElementState{Key: key, Id: id, ElementType: ExclusiveGatewayType}, Handler: handler}
}

func (eg *ExclusiveGateway) GetElementState() ElementState {
	return eg.ElementState
}

func (eg *ExclusiveGateway) RefElement() any {
	return eg
}

func (eg *ExclusiveGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Completing(ctx, bpmnEngine, token)
	eg.Completed(ctx, bpmnEngine, token)
}

func (eg *ExclusiveGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleting
	bpmnEngine.WriteState(eg.ElementState)
	eg.NextElement = eg.Handler()
}

func (eg *ExclusiveGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	eg.Status = ElementCompleted
	bpmnEngine.WriteState(eg.ElementState)
}

type ScriptTask struct {
	ElementState
	script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)
}

func NewScriptTask(key, id string, script func(ctx context.Context, bpmnEngine *BPMNEngine, token *Token, task *ScriptTask)) *ScriptTask {
	return &ScriptTask{ElementState: ElementState{Key: key, Id: id, ElementType: ScriptTaskType}, script: script}
}

func (st *ScriptTask) GetElementState() ElementState {
	return st.ElementState
}

func (st *ScriptTask) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Completing(ctx, bpmnEngine, token)
	st.Completed(ctx, bpmnEngine, token)
}

func (st *ScriptTask) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementCompleting
	bpmnEngine.WriteState(st.ElementState)
	st.script(ctx, bpmnEngine, token, st)
}

func (st *ScriptTask) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	st.Status = ElementCompleted
	bpmnEngine.WriteState(st.ElementState)
}

type NoneIntermediateThrowEvent struct {
	ElementState
}

func NewNoneIntermediateThrowEvent(key, id string) *NoneIntermediateThrowEvent {
	return &NoneIntermediateThrowEvent{ElementState: ElementState{Key: key, Id: id, ElementType: NoneIntermediateThrowEventType}}
}

func (e *NoneIntermediateThrowEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneIntermediateThrowEvent) RefElement() any {
	return e
}

func (e *NoneIntermediateThrowEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	e.Status = ElementCompleted
	bpmnEngine.WriteState(e.ElementState)
}

type NoneEndEvent struct {
	ElementState
}

func NewNoneEndEvent(key, id string) *NoneEndEvent {
	return &NoneEndEvent{ElementState: ElementState{Key: key, Id: id, ElementType: NoneEndEventType}}
}

func (e *NoneEndEvent) GetElementState() ElementState {
	return e.ElementState
}

func (e *NoneEndEvent) RefElement() any {
	return e
}

func (e *NoneEndEvent) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	e.Status = ElementCompleted
	bpmnEngine.WriteState(e.ElementState)
}

type BaseTask struct {
	CompletedBy
	Comments
}

type CompletedBy struct {
	CompletedById   string
	CompletedByUser string
}

type Comments struct {
	Comments string
}

type UserTask[T any] struct {
	ElementState
	completed     bool
	CompletedTime time.Time
	Data          T
}

type GetUserTaskCmd struct {
}

type CompleteUserTaskCmd struct {
	ProcessInstanceId string
	TaskId            string
	Data              any
}

type CompleteUserTaskResp struct {
	ProcessInstanceId string
	TaskId            string
	Error             error
}

func NewUserTask[T any](key, id string) *UserTask[T] {
	task := UserTask[T]{ElementState: ElementState{Key: key, Id: id, ElementType: UserTaskType}}
	task.Object = &task
	return &task
}

func (t *UserTask[T]) GetElementState() ElementState {
	return t.ElementState
}

func (t *UserTask[T]) RefElement() any {
	return t
}

func (t *UserTask[T]) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	t.Completing(ctx, bpmnEngine, token)
	t.Completed(ctx, bpmnEngine, token)
}

func (t *UserTask[T]) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	t.Status = ElementCompleting
	bpmnEngine.WriteState(t.ElementState)
	t.RunTask(ctx, bpmnEngine, token)
}

func (t *UserTask[T]) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	t.Status = ElementCompleted
	bpmnEngine.WriteState(t.ElementState)
}

func (t *UserTask[T]) RunTask(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	token.setAcceptingCommands(true)
	defer func() {
		token.setAcceptingCommands(false)
	}()
	for {
		select {
		case <-ctx.Done():
			return
		case cmd := <-token.cmdReq:
			switch c := cmd.(type) {
			case CompleteUserTaskCmd:
				resp := CompleteUserTaskResp{t.Id, token.ProcessInstance.Id, nil}
				if c.TaskId == t.Id && c.ProcessInstanceId == token.ProcessInstance.Id {
					// Task Complete
					t.completed = true
					t.CompletedTime = time.Now()
					data, ok := c.Data.(T)
					if ok {
						t.Data = data
					} else {
						err := fmt.Errorf("unable to marshal data to type %T", t.Data)
						resp.Error = NewBPMNEngineError(err, "completeUserTaskCmd error")
						log.Println(resp.Error)
					}
					token.cmdResp <- resp
					return
				}
				token.cmdResp <- resp
			case GetUserTaskCmd:
				pi := token.ProcessInstance
				token.cmdResp <- TaskState{
					ProcessInstanceKey:     pi.Key,
					ProcessInstanceId:      pi.Id,
					ProcessInstanceVersion: pi.Version,
					Key:                    t.Key,
					Id:                     t.Id,
					Status:                 t.Status,
					Data:                   t.Data, // Todo: Copy data
				}
			}
		}
	}
}

func (t *UserTask[T]) Clone() *UserTask[T] {
	newTask := UserTask[T]{ElementState: ElementState{
		Key:         t.Key,
		Id:          t.Id,
		ElementType: t.ElementType,
		Status:      t.Status,
		//Created:     t.Created, //TODO: ADD
	},
		completed:     t.completed,
		CompletedTime: t.CompletedTime,
	}

	return &newTask
}

type ParallelGatewayCmd struct {
	pg ParallelGateway
}

type ParallelGateway struct {
	ElementState
	CurrentSeqKey string
	InSeqKeys     []string
	OutSeqKeys    []string
}

func NewParallelGateway(key, id, currentSeqKey string, inSeqKeys, outSeqKeys []string) *ParallelGateway {
	return &ParallelGateway{
		ElementState:  ElementState{Key: key, Id: id, ElementType: ParallelGatewayType},
		CurrentSeqKey: currentSeqKey,
		InSeqKeys:     inSeqKeys,
		OutSeqKeys:    outSeqKeys,
	}
}

func (pg *ParallelGateway) GetElementState() ElementState {
	return pg.ElementState
}

func (pg *ParallelGateway) RefElement() any {
	return pg
}

func (pg *ParallelGateway) RunLifecycle(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	pg.Completing(ctx, bpmnEngine, token)
	pg.Completed(ctx, bpmnEngine, token)
}

func (pg *ParallelGateway) Completing(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	pg.Status = ElementCompleting
	bpmnEngine.WriteState(pg.ElementState)
	for {
		resp, err := token.ProcessInstance.SendCommand(*pg)
		if err != nil {
			fmt.Printf("parallel gateway: %v\n", err)
			time.Sleep(1 * time.Millisecond)
			continue
		}
		done, ok := resp.(<-chan any)
		if !ok {
			fmt.Printf("parallel gateway: completing received: %v", done)
			time.Sleep(1 * time.Millisecond)
			continue
		}
		<-done
		break
	}
}

func (pg *ParallelGateway) Completed(ctx context.Context, bpmnEngine *BPMNEngine, token *Token) {
	pg.Status = ElementCompleted
	bpmnEngine.WriteState(pg.ElementState)
}
