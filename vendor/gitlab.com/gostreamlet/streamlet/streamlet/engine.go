package streamlet

import (
	"context"
	"errors"
	"fmt"
	"runtime/debug"
	"sync"
	"time"
)

var (
	ErrBPMNEngineNotStarted                 = NewBPMNEngineError(nil, "engine: not started.")
	ErrBPMNEngineCancelled                  = NewBPMNEngineError(nil, "engine: cancelled.")
	ErrBPMNEngineIdGeneratorNotSet          = NewBPMNEngineError(nil, "engine: Id Generator not set.")
	ErrBPMNEngineProcessInstanceStoreNotSet = NewBPMNEngineError(nil, "engine: Process Instance Store not set.")
	ErrBPMNEngineTokenStoreNotSet           = NewBPMNEngineError(nil, "engine: Token Store not set.")
	ErrBPMNEngineStateStoreNotSet           = NewBPMNEngineError(nil, "engine: State Store not set.")
	ErrBPMNEngineStateHistoryStoreNotSet    = NewBPMNEngineError(nil, "engine: State History Store not set.")
	ErrBPMNEngineSendCommandTimeout         = NewBPMNEngineError(nil, "engine: send command timeout.")
	ErrBPMNEngineReceiveCommandTimeout      = NewBPMNEngineError(nil, "engine: receive command timeout.")
	ErrBPMNEngineProcessInstanceNotFound    = NewBPMNEngineError(nil, "engine: process instance not found.")
	ErrBPMNEngineProcessInstanceNotRunning  = NewBPMNEngineError(nil, "engine: process instance not running.")
)

type BPMNEngine struct {
	IdGenerator          IdGenerator                 // Generates ids for elements
	StartEventProcessors []StartEventProcessor       // Start Event Processors to run on startup
	ProcessInstanceStore ProcessInstanceStore        // Stores process instance states received from state stream
	TokenStore           TokenStore                  // Stores token states received from state stream
	StateStore           StateStore                  // Stores states received from stateStream
	StateHistoryStore    StateHistoryStore           // Stores history of states
	ctx                  context.Context             // Context for the workload engine
	cancel               context.CancelFunc          // Used to cancel running the workload engine
	cmdReq               chan any                    // Commands to the engine are sent on the request channel
	cmdResp              <-chan any                  // Responses to commands are sent on the response channel
	wgProcessInstance    sync.WaitGroup              // Wait group to wait for process instance goroutines to finish
	wgStateStream        sync.WaitGroup              // Wait group to wait for the state stream goroutine to finish
	stateStream          chan any                    // Channel for state changes
	processInstances     map[string]*ProcessInstance // Maps a process instance Id to the process instance
	mu                   sync.RWMutex
}

// NewBPMNEngine creates a new BPMNEngine with given IdGenerator, StateStore, and StateHistoryStore
func NewBPMNEngine(g IdGenerator, pis ProcessInstanceStore, ts TokenStore, ss StateStore, shs StateHistoryStore) *BPMNEngine {
	ctx, cancel := context.WithCancel(context.Background())
	b := BPMNEngine{
		IdGenerator:          g,
		StartEventProcessors: make([]StartEventProcessor, 0),
		ProcessInstanceStore: pis,
		TokenStore:           ts,
		StateStore:           ss,
		StateHistoryStore:    shs,
		ctx:                  ctx,
		cancel:               cancel,
		processInstances:     make(map[string]*ProcessInstance),
		stateStream:          make(chan any),
		wgProcessInstance:    sync.WaitGroup{},
		wgStateStream:        sync.WaitGroup{},
	}
	return &b
}

// NewMemoryBPMNEngine creates a new BPMNEngine with an in-memory id generator, state store, and history store.
func NewMemoryBPMNEngine() *BPMNEngine {
	g := NewMemoryIdGenerator()
	s := NewMemoryStateStore()
	b := NewBPMNEngine(g, s, s, s, NewMemoryStateHistoryStore())
	g.Start(b.ctx)
	return b
}

type StartEventProcessor interface {
	StartEventProcesses(ctx context.Context)
}

func (b *BPMNEngine) AddStartEventProcesser(processor StartEventProcessor) {
	b.StartEventProcessors = append(b.StartEventProcessors, processor)
}

// Start starts the engine, and starts routines to receive state and commands
func (b *BPMNEngine) Start() error {
	if b.IdGenerator == nil {
		return ErrBPMNEngineIdGeneratorNotSet
	}

	if b.ProcessInstanceStore == nil {
		return ErrBPMNEngineProcessInstanceStoreNotSet
	}

	if b.TokenStore == nil {
		return ErrBPMNEngineTokenStoreNotSet
	}

	if b.StateStore == nil {
		return ErrBPMNEngineStateStoreNotSet
	}

	if b.StateHistoryStore == nil {
		return ErrBPMNEngineStateHistoryStoreNotSet
	}

	b.startStateStreamRoutine()
	b.startCommandRoutine()
	b.startStartEventProcessors()

	return nil
}

func (b *BPMNEngine) startCommandRoutine() {
	cmdFunc := func(done <-chan struct{}, reqChan <-chan any) <-chan any {
		respCh := make(chan any)
		go func() {
			defer func() {
				close(respCh)
			}()
			for {
				select {
				case <-done:
					return
				case data := <-reqChan:
					switch v := data.(type) {
					case StartProcessInstanceCmd:
						err := b.startProcessInstance(v.Instance)
						respCh <- StartProcessInstanceResp{Err: err}
					case GetProcessInstancesCmd:
						respCh <- *b.getProcessInstances()
					case GetTasksCmd:
						respCh <- *b.getTasks()
					case CompleteUserTasksCmd:
						respCh <- *b.completeTasks(v)
					case ProcessInstanceCmd:
						resp, err := b.sendProcessInstanceCmd(v)
						if err != nil {
							respCh <- err
						}
						respCh <- resp
					case processCompletedCmd:
						b.mu.Lock()
						delete(b.processInstances, v.Id)
						b.mu.Unlock()
						respCh <- v
					default:
						respCh <- v // Todo: Implement commands
					}
				}
			}
		}()
		return respCh
	}

	b.cmdReq = make(chan any)
	b.cmdResp = cmdFunc(b.ctx.Done(), b.cmdReq)
}

func (b *BPMNEngine) startStateStreamRoutine() {
	// Start Go Routine for Reading and Writing State
	b.wgStateStream.Add(1)
	go func(ctx context.Context) {
		defer b.wgStateStream.Done()
		for {
			select {
			case <-ctx.Done():
				return
			case state, ok := <-b.stateStream:
				if ok {
					switch s := state.(type) {
					case ElementState:
						b.StateStore.WriteState(s)
						b.StateHistoryStore.WriteStateHistory(s)
					case ProcessInstanceState:
						b.ProcessInstanceStore.WriteProcessInstanceState(s)
					case TokenState:
						b.TokenStore.WriteToken(s)
					}
				} else {
					return
				}
			}
		}
	}(b.ctx)
}

// SendCommand sends any command to a process instance
func (b *BPMNEngine) SendCommand(cmd any) (any, error) {
	return b.SendCommandWithTimeout(cmd, 30000*time.Millisecond)
}

func (b *BPMNEngine) SendCommandWithTimeout(cmd any, timeout time.Duration) (any, error) {
	if b.ctx == nil || b.cmdReq == nil || b.cmdResp == nil {
		return nil, ErrBPMNEngineNotStarted
	}
	select {
	case <-b.ctx.Done():
		err := ErrBPMNEngineCancelled
		err.Message += fmt.Sprintf(" \n\t%#v", cmd)
		return nil, err
	case b.cmdReq <- cmd:
		return <-b.cmdResp, nil
	case <-time.After(timeout):
		err := ErrBPMNEngineSendCommandTimeout
		err.Message += fmt.Sprintf(" %#v", cmd)
		return nil, err
	}
}

// Wait waits for all process instances to complete
func (b *BPMNEngine) Wait() {
	b.wgProcessInstance.Wait()
	for {
		if b.ProcessInstanceCount() > 0 {
			time.Sleep(1 * time.Millisecond)
		} else {
			return
		}
	}
}

// Stop stops the engine
func (b *BPMNEngine) Stop() {
	close(b.stateStream)
	b.wgStateStream.Wait()
	b.cancel()
}

func (b *BPMNEngine) WriteProcessInstanceState(state ProcessInstanceState) {
	b.stateStream <- state
}

func (b *BPMNEngine) WriteToken(state TokenState) {
	b.stateStream <- state
}

// WriteState writes element state to the state store and history store
func (b *BPMNEngine) WriteState(state ElementState) {
	b.stateStream <- state
}

// GenerateId generates a Pid for a given element type
func (b *BPMNEngine) GenerateId(elementType ElementType) string {
	return b.IdGenerator.GenerateId(elementType)
}

// startProcessInstance starts a process instance in a new go routine
func (b *BPMNEngine) startProcessInstance(instance *ProcessInstance) error {
	// Increase wait group for new go routine for process instance
	b.wgProcessInstance.Add(1)

	// Run the process instance. Starts a go routine
	err := instance.Run(b.ctx, &b.wgProcessInstance)
	if err != nil {
		return err
	}

	// Add the process instance for future reference
	// Must be called after run, otherwise the process instance could receive a command before the context is set.
	b.mu.Lock()
	b.processInstances[instance.Id] = instance
	b.mu.Unlock()
	return nil
}

// getProcessInstances returns information on all running process instances
func (b *BPMNEngine) getProcessInstances() *GetProcessInstancesResp {
	resp := NewGetProcessInstancesResp()
	b.mu.Lock()
	pis := make([]*ProcessInstance, 0, len(b.processInstances))
	for i := range b.processInstances {
		pis = append(pis, b.processInstances[i])
	}
	b.mu.Unlock()

	for i := range pis {
		p := pis[i]
		if !p.Running() {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: errors.New("engine: process not running")})
			continue
		}
		pResp, err := p.SendCommand(GetProcessInstanceInfoCmd{})
		if err != nil {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: err})
			continue
		}
		pInfo, ok := pResp.(ProcessInstanceState)
		if !ok {
			resp.Errors = append(resp.Errors, BPMNEngineError{Inner: errors.New("engine: GetProcessInstanceInfoCmd response is not type ProcessInstanceState")})
			continue
		}
		resp.Data = append(resp.Data, pInfo)
	}
	return resp
}

// getTasks returns information on all running tasks
func (b *BPMNEngine) getTasks() *GetTasksResp {
	resp := NewGetTasksResp()
	b.mu.Lock()
	pis := make([]*ProcessInstance, 0, len(b.processInstances))
	for i := range b.processInstances {
		pis = append(pis, b.processInstances[i])
	}
	b.mu.Unlock()
	for _, p := range pis {
		if !p.Running() {
			continue
		}
		tResp, err := p.SendCommand(GetTasksInfoCmd{})
		if err != nil {
			resp.Errors = append(resp.Errors, err)
			continue
		}
		tInfos, ok := tResp.([]TaskState)
		if !ok {
			continue
		}
		for _, tInfo := range tInfos {
			resp.Data = append(resp.Data, tInfo)
		}
	}
	return resp
}

// completeTasks completes the requested tasks
func (b *BPMNEngine) completeTasks(v CompleteUserTasksCmd) *CompleteTasksResp {
	//log.Println("Complete tasks")
	resp := NewCompleteTasksResp()
	for _, tc := range v.Tasks {
		p, ok := b.processInstances[tc.ProcessInstanceId]
		if !ok {
			fmt.Println("Could not find process instance with id: ", tc.ProcessInstanceId)
			continue
		}
		if !p.Running() {
			continue
		}
		tResp, err := p.SendCommand(tc)
		if err != nil {
			resp.Errors = append(resp.Errors, err)
		}
		if tResp != nil {
			tasks := tResp.(CompleteTasksResp)
			for _, t := range tasks.TasksResp {
				resp.TasksResp = append(resp.TasksResp, t)
			}
			for _, e := range tasks.Errors {
				resp.Errors = append(resp.Errors, e)
			}
		}
	}
	return resp
}

// sendProcessInstanceCmd sends a command to a specific process instance
func (b *BPMNEngine) sendProcessInstanceCmd(cmd ProcessInstanceCmd) (any, error) {
	p, ok := b.processInstances[cmd.Id]
	if !ok {
		fmt.Println("Could not find process instance with id: ", cmd.Id)
		return nil, ErrBPMNEngineProcessInstanceNotFound
	}
	if !p.Running() {
		return nil, ErrBPMNEngineProcessInstanceNotRunning
	}
	return p.SendCommand(cmd.Cmd)
}

func (b *BPMNEngine) ProcessInstanceCount() int {
	b.mu.RLock()
	defer b.mu.RUnlock()
	return len(b.processInstances)
}

func (b *BPMNEngine) startStartEventProcessors() {
	for _, s := range b.StartEventProcessors {
		s.StartEventProcesses(b.ctx)
	}
}

type BPMNEngineError struct {
	Inner      error
	Message    string
	StackTrace string
	Misc       map[string]any
}

func NewBPMNEngineError(err error, messagef string, msgArgs ...any) BPMNEngineError {
	return BPMNEngineError{
		Inner:      err,
		Message:    fmt.Sprintf(messagef, msgArgs...),
		StackTrace: string(debug.Stack()),
		Misc:       make(map[string]any),
	}
}

func (err BPMNEngineError) Error() string {
	return err.Message
}
