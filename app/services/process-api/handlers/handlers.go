package handlers

import (
	"context"
	"embed"
	"encoding/json"
	"fmt"
	"gitlab.com/gostreamlet/streamlet/streamlet"
	"gitlab.com/gostreamlet/web/foundation/web"
	"html/template"
	"net/http"
	"sort"
	"strings"
)

// content holds our static web server content
//
//go:embed templates/*
var content embed.FS

type APIMuxConfig struct {
	Engine          *streamlet.BPMNEngine
	ProcessHandlers []ProcessHandler
}

func APIMux(cfg APIMuxConfig) *web.App {
	var app *web.App
	app = web.NewApp()
	peh := NewProcessEngineHandlers(cfg.Engine)
	for _, h := range cfg.ProcessHandlers {
		group := strings.ToLower(h.ProcessKey)
		app.Handle(http.MethodPost, group, "/start", h.StartHandler)
		app.Handle(http.MethodGet, group, "/processinstances", peh.getProcessInstances)
		app.Handle(http.MethodGet, group, "/tasks", peh.getTasks)
		app.Handle(http.MethodPost, group, "/tasks/complete", h.CompleteTaskHandler)
		app.Handle(http.MethodGet, group, "/debug/processinstances", peh.debugProcessInstances)
		app.Handle(http.MethodGet, group, "/debug/tasks", peh.debugTasks)
	}
	return app
}

type ProcessHandler struct {
	Engine              *streamlet.BPMNEngine
	ProcessKey          string
	StartHandler        web.Handler
	CompleteTaskHandler web.Handler
}

func NewProcessEngineHandlers(engine *streamlet.BPMNEngine) *ProcessEngineHandlers {
	h := ProcessEngineHandlers{engine: engine}

	// # Process Instances
	bytes, err := content.ReadFile("templates/processinstances.html")
	if err != nil {
		fmt.Printf("error reading processinstance.html: %v\n", err)
	}
	h.DebugProcessInstanceTemplate, err = template.New("ProcessInstances").Parse(string(bytes))
	if err != nil {
		fmt.Printf("error parsing template: %v", err)
	}

	// # Tasks
	bytes, err = content.ReadFile("templates/tasks.html")
	if err != nil {
		fmt.Printf("error reading tasks.html: %v\n", err)
	}
	h.DebugTaskTemplate, err = template.New("Tasks").Parse(string(bytes))
	if err != nil {
		fmt.Printf("error parsing template: %v", err)
	}
	return &h
}

type ProcessEngineHandlers struct {
	engine                       *streamlet.BPMNEngine
	DebugProcessInstanceTemplate *template.Template
	DebugTaskTemplate            *template.Template
}

func (p ProcessEngineHandlers) getProcessInstances(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	result, err := p.engine.SendCommand(streamlet.GetProcessInstancesCmd{})
	if err != nil || result == nil {
		fmt.Println(err)
		http.Error(w, "Error getting process instances", 500)
	}
	var resp streamlet.GetProcessInstancesResp
	resp, ok := result.(streamlet.GetProcessInstancesResp)
	if !ok {
		return nil
	}
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		http.Error(w, "Error marshalling response", 500)
		fmt.Println(err)
	}
	return nil
}

func (p ProcessEngineHandlers) debugProcessInstances(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	result, err := p.engine.SendCommand(streamlet.GetProcessInstancesCmd{})
	if err != nil || result == nil {
		fmt.Println(err)
		http.Error(w, "Error getting process instances", 500)
	}
	var resp streamlet.GetProcessInstancesResp
	resp, ok := result.(streamlet.GetProcessInstancesResp)
	if !ok {
		return nil
	}
	sort.Slice(resp.Data, func(i, j int) bool {
		return resp.Data[i].Created.Before(resp.Data[j].Created)
	})
	return p.DebugProcessInstanceTemplate.Execute(w, resp)
}

func (p ProcessEngineHandlers) getTasks(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	w.Header().Set("Content-Type", "application/json")
	result, err := p.engine.SendCommand(streamlet.GetTasksCmd{})
	if err != nil || result == nil {
		http.Error(w, "Error getting tasks", 500)
	}
	var resp streamlet.GetTasksResp
	resp = result.(streamlet.GetTasksResp)
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		http.Error(w, "Error marshalling response", 500)
		fmt.Println(err)
	}
	return nil
}

func (p ProcessEngineHandlers) debugTasks(ctx context.Context, w http.ResponseWriter, req *http.Request) error {
	result, err := p.engine.SendCommand(streamlet.GetTasksCmd{})
	if err != nil || result == nil {
		http.Error(w, "Error getting tasks", 500)
	}
	var resp streamlet.GetTasksResp
	resp = result.(streamlet.GetTasksResp)
	return p.DebugTaskTemplate.Execute(w, resp)
}
