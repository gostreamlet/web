# Streamlet Web
The streamlet web project enables creating web services for streamlet BPMN processes.
The goal is to enable out of the box web services for an executable BPMN process. Additionally,
 it should enable customization as needed for specific processes with additional backend, debugging, and other end points.

It provides backend APIs for the following:
- Starting process instances
- Listing running process instances
- Listings available tasks
- Completing tasks

It also provides a simple debug web interface for the following:
- Listing running process instances
- Listing available tasks
- Limited support for completing tasks

## Getting Started
Streamlet web uses a command line tool to generate the go code for a streamlet web service.

1. Install the stream service `strmsrv` command:
```shell
go install gitlab.com/gostreamlet/web/app/cmd/strmsrv
```
2. Install dependencies:
```shell
go get gitlab.com/gostreamlet/web
```
3. From the root of a streamlet project:
```bash
strmsrv init
```
3. Start the web service for the process:
```bash
go run app\service\main.go
```
The service listens by default on `localhost:8081`

## REST API
The following endpoints are exposed by default for each process definition id:
- Start a process instance: `[POST]/[process definition id]/start`
- Get a list of current process instances: `[GET]/[process definition id]/processinstances`
- Get a list of tasks: `[GET]/[process definition id]/tasks`
- Comple a task `[POST]/[process definition id]/tasks/complete`
- Debug page listing process instances: `[GET]/[process definition id]/debug/processinstances`
- Debug page listing tasks: `[GET]/[process definition id]/debug/tasks`

## Examples
Detailed examples may be found in the examples project:

https://gitlab.com/gostreamlet/examples

## License
License information to come.